goed
====

This project is not yet usable. This project is an attempt to create a Go-specific editor from scratch with inspiration from Emacs and Godit (nsf).

This project was undertaken because I personally found all popular editors to be excessively cumbersome, antiquated, or ill-suited to Go programming. Combined with the general desire to write my own editor, I'm writing my own editor.

I would like to give credit where credit is due:                                            
* GNU Emacs and its general awesomeness                                                     
* Godit and No Smile Face (NSF, github.com/nsf)                                             

I didn't look at the Emacs code, but I'm a fan of its versatility. I hope to mimic that    
versatility some point down the road.

I did look at the Godit code, especially while getting started. NSF wrote the termbox-go package, which I used, and wrote an editor using that package, the combination making a fantastic reference.

You'll probably see inspiration from other editors as time passes with varying degrees of subtlety.

Keyboard Commands
-----------------

Effort has been made to make the keyboard commands consistent and fairly familiar. Most of the command set is based on command sets that are in common use today. For example, see 'Sacred Keybindings' (https://en.wikipedia.org/wiki/Keyboard_shortcut#.22Sacred.22_keybindings) and 'Table of Keyboard Shortcuts' (https://en.wikipedia.org/wiki/Table_of_keyboard_shortcuts) on Wikipedia.

At some point, I think it would be useful to include some level of customization, but I also think there is great value to having a command set that is familiar to anyone sitting down at the editor. I would be open to suggestions regarding how to best accomplish this.

Furthermore, I am considering having some kind of consistent convention for program/global commands versus buffer commands. For example any command with the Ctrl key would be commands whose scope is limited to within the boundaries of the buffer and commands with the Alt key would be program commands, ie commands for resizing/moving buffers, saving all buffers, quitting, creating a new buffer, etc.

Command | Operation            | Comment
--------|----------------------|--------
C-/     | Comment line(s)      | Block or line comment per context.
C-a     | Select all           | 
C-c     | Copy                 | 
C-f     | Find                 | 
C-g     | Find again           | 
C-n     | New buffer           | 
C-o     | Open file            | Always in a new buffer.
C-q     | Quit program         |
C-r     | Refresh from disk    | 'Revert' for Emacs users.
C-s     | Save to file         | 
C-v     | Paste                | 
C-w     | Close buffer         | 
C-x     | Cut                  | 
C-y     | Redo                 | 
C-z     | Undo                 | 
C-space | toggle highlighting  | Starts on and includes the cursor highlight.
C-left  | Previous word        | Always lands on the first letter of the word.
C-right | Next word            | Always lands on the first letter of the word.
C-up    | Previous line break  | Always lands on the first rune after the line break.
C-down  | Next line break      | Always lands on the first rune after the line break.
C-home  | Beginning of file    | Always lands on the first rune of the file.
C-end   | End of file          | Always lands on the last rune of the file.
C-tab   | Next buffer          |
