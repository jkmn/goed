package main

import (
	termbox "github.com/nsf/termbox-go"
	"log"
	"os"
)

var (
	buffers  []*Buffer
	events   chan termbox.Event = make(chan termbox.Event)
	sequence []uint16           = make([]uint16, 0)
)

func init() {}

func main() {
	var err error

	// Initialize termbox.
	if err = termbox.Init(); err != nil {
		panic(err)
	}
	defer termbox.Close()
	termbox.SetInputMode(termbox.InputAlt)

	// Setup logging.
	var file *os.File
	file, err = os.Create("goed.log")
	if err != nil {
		panic(err)
	}
	log.SetOutput(file)
	log.Println("Logging started.")

	// Load buffers.
	for _, arg := range os.Args[1:] {
		b := new(Buffer)
		err = b.LoadFile(arg)
		if err == nil {
			buffers = append(buffers, b)
			err = nil
		}
	}

	// Set up the first frame.
	w, h := termbox.Size()
	x_ := 0
	y_ := 0
	w_ := w
	h_ := h
	f := NewFrame(x_, y_, w_, h_)
	if len(buffers) > 0 {
		f.Load(buffers[0])
	}

	f.Draw()

	go collectEvent()

	// Keep accepting events until handleEvent() says otherwise.
	for {
		e := <-events
		running := handleEvent(e)
		if !running {
			break
		}
	}
}

func collectEvent() {
	for {
		events <- termbox.PollEvent()
	}
}

/*
This event handler catches events that require global state change
and then passes any others into the active buffer.
Returns true iff the program should continue to run.
*/
func handleEvent(e termbox.Event) bool {

	if len(sequence) > 0 {
		return handleEventSequence(e)
	}

	// C-n: New buffer
	// C-o: Open file
	// C-w: Close buffer
	// C-tab: Next buffer

	if e.Ch == 0 {
		if e.Key == termbox.KeyCtrlQ {
			return false
		}
	}

	if e.Ch == 'q' {
		return false
	}

	return true
}

func handleEventSequence(e termbox.Event) bool {

	return true
}
