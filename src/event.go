package main

import (
	termbox "github.com/nsf/termbox-go"
)

type Event struct {
	termbox.Event
	X, Y int // Location of the event on the screen.
}
