package main

import (
	"bufio"
	termbox "github.com/nsf/termbox-go"
	"os"
)

// Buffer, like an Emacs buffer.
type Buffer struct {
	name    string   // Name of the buffer. Shown in status bar.
	path    string   // Complete path of the file loaded in the buffer.
	row     int      // The row location of the cursor.
	col     int      // The column location of the cursor on the present line.
	lastCol int      // The last column selected upon horizontal navigation.
	rows    []string // The data of the file parsed into lines.
	//	data             []byte // The loaded data, presumably from a file.
}

func (b *Buffer) HandleEvent(e Event) error {

	switch e.Key {
	case termbox.KeyArrowDown:
		b.row++
		b.fixCursor()
	case termbox.KeyArrowUp:
		b.row--
		b.fixCursor()
	case termbox.KeyArrowLeft:
		b.col--
		b.fixCursor()
	case termbox.KeyArrowRight:
		b.col++
		b.fixCursor()
	}

	// C-/: Comment line(s)
	// C-a: Select all
	// C-c: Copy
	// C-f: Find
	// C-g: Find again
	// C-r: Discard changes, revert buffer
	// C-s: Save to file
	// C-v: Paste
	// C-x: Cut
	// C-y: Redo
	// C-z: Undo

	// C-space: Toggles highlighting
	// Starts on and includes the cursor highlight.

	// C-left: Previous word
	// Always lands on the first letter of the word.

	// C-right: Next word
	// Always lands on the first letter of the word.

	// C-up: Previous line break
	// Always lands on the first rune after the line break.

	// C-down: Next line break
	// Always lands on the first rune after the line break.

	// C-home: Beginning of file
	// Always lands on the first rune of the file.

	// C-end: End of file
	// Always lands on the last rune of the file.

	return nil
}

/*
In reality, you're probably never going to load a file without rendering it, which is how this method works,
but I hate side effects. Remember to render/draw the buffer after you call this method or you won't see any changes.
*/
func (b *Buffer) LoadFile(path string) error {
	var (
		err   error
		file  *os.File
		lines []string = make([]string, 0)
	)

	file, err = os.Open(path)
	if err != nil {
		return err
	}

	scnr := bufio.NewScanner(file)
	for scnr.Scan() != false {
		lines = append(lines, scnr.Text())
	}

	err = scnr.Err()

	return err
}

func (b *Buffer) Name() string {
	return b.name
}

/*
Return the lines between the lower and upper limit.
While typically all of the buffer needs to be evaluated for syntax highlighting, we don't need to return all of the
buffer to the frame, just the lines it's requesting.
*/
func (b *Buffer) Lines(start, end int) []*Line {

	var (
		strings []string = make([]string, 0) // unformatted lines
		lines   []*Line  = make([]*Line, 0)  // formatted lines
	)

	for i := start; i < end; i++ {
		strings = append(strings, b.rows[i])
	}

	// Format the text.
	// In the future, this might be a good place to put syntax highlight, error highlighting, etc.
	for _, s := range strings {
		line := NewLine(s)
		lines = append(lines, line)
	}

	return lines
}

func (b *Buffer) fixCursor() {

}

func (b *Buffer) Cursor() (row int, col int) {
	row = b.row
	col = b.col
	return
}
