/*
This project was undertaken because I personally found all popular editors to be excessively
cumbersome, antiquated, or ill-suited to Go programming. Combined with the general desire to
write my own editor, I'm writing my own editor.

I would like to give credit where credit is due:
* GNU Emacs and its general awesomeness
* Godit and No Smile Face (NSF, github.com/nsf)


 I didn't look at the Emacs code, but I'm a fan of its versatility. I hope to mimic that
versatility some point down the road.

I did look at the Godit code, especially while getting started. NSF just so happened to use
the same library that I found and wanted to use for terminal interaction.
*/
package main
