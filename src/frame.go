package main

import (
	termbox "github.com/nsf/termbox-go"
	"log"
)

/*
A frame contains a single object fulfilling the Displayable contract, usually a Buffer. It can switch Displayables
at the users request. In other environments, it is mostly equivalent to a Window. It maintains geometic constraints
for rendering a Displayable. A frame reserves one line at the bottom for status information.

In terms of division of duties, a frame knows which part of the buffer it's looking at, but doesn't care what the
buffer is or how it operates. It passes events to the buffer with appropriate coordinate transformations so that the
buffer doesn't have to know about anything outside of itself.

A frame is responsible for rendering itself via termbox.
*/
type Frame struct {
	Rect
	content Displayable
	loaded  bool // Set when content is loaded or unset when unloaded.
	offsetY int  // Y offset, the first buffer row visible in the frame.
	offsetX int  // X offset, the first buffer column visible in the frame.
	cursorX int  // Horizontal position of cursor within the frame.
	cursorY int  // Vertical position of the cursor within the frame.
}

// Simply put the cursor in the appropriate place.
func (f *Frame) setCursor() {
	x := f.cursorX
	y := f.cursorY

	// Check to ensure the buffer is within bounds.
	if x < 0 {
		x = 0
	} else if x >= f.W {
		x = f.W - 1
	}
	if y < 0 {
		y = 0
	} else if y >= f.H {
		y = f.H - 1
	}

	x = x + f.X
	y = y + f.X

	termbox.SetCursor(x, y)
}

func (f *Frame) Load(content Displayable) {
	f.loaded = true
	f.content = content
}

func (f *Frame) Unload() {
	f.loaded = false
	f.content = nil
}

// Push the complete state of the buffer to the terminal and syncronize/flush.
func (f *Frame) Draw() {

	if !f.loaded {
		return
	}

	// Draw the buffer data.
	lines := f.content.Lines(f.offsetY, f.offsetY+f.H)

	var li int = 0 // line index
	for row, line := range lines {
		li++
		for col, r := range line.Rouns {
			x := f.X + col
			y := f.Y + row
			f.setCell(x, y, r.R, r.FG, r.BG)
		}
	}

	// Set the cursor.
	f.setCursor()

	termbox.Flush()
}

// Create a new buffer with the given dimensions.
func NewFrame(x, y, w, h int) *Frame {
	f := new(Frame)
	f.X = x
	f.Y = y
	f.W = w
	f.H = h
	return f
}

/*
Wrapper for termbox SetCell(), but does coordinate translation with respect to the buffer.
Runes cannot be printed outside of the bounds of the buffer.
Because this can be called many times in rapid succession, flush is not called.
*/
func (f *Frame) setCell(x, y int, ch rune, fg, bg termbox.Attribute) {
	// Check to ensure the location is within bounds.
	if (x < 0) || (y < 0) {
		log.Printf("Under bounds: (%d,%d)", x, y)
		return
	}
	if (x >= f.W) || (y >= f.H) {
		log.Printf("%c out of bounds (%d,%d): (%d,%d)", ch, f.W, f.H, x, y)
		return
	}

	// Translate location.
	x = x + f.X
	y = y + f.X

	termbox.SetCell(x, y, ch, fg, bg)
}
