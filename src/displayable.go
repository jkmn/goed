package main

import (
	termbox "github.com/nsf/termbox-go"
)

type Displayable interface {
	Name() string                 // Name of the object.
	Lines(start, end int) []*Line // Returns the lines in the given range.
	HandleEvent(Event) error      // Let object handle its own events.
	Cursor() (row int, col int)   // Returns the cursor position.
	Ready() bool
}

// This is a very simple object, but defining it this way allows us to deal with it much more gracefully in code.
type Line struct {
	Rouns []Roun
}

func NewLine(s string) *Line {
	runes := []rune(s)
	l := new(Line)
	for _, r := range runes {
		roun := Roun{
			FG: termbox.ColorWhite,
			BG: termbox.ColorBlack,
			R:  r,
		}
		l.Rouns = append(l.Rouns, roun)
	}
	return l
}

func (l *Line) AddChunk(s string, fg, bg termbox.Attribute) {}

func (l *Line) String() string {
	var runes []rune

	for _, r := range l.Rouns {
		runes = append(runes, r.R)
	}
	return string(runes)
}

type Roun struct {
	FG termbox.Attribute
	BG termbox.Attribute
	R  rune
}
